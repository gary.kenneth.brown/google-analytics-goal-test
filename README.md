# Google Analytics Goal Test

## Getting started

Proof of concept project to try out scenarios with Google Analytics (GA). We want to be able to track conversions and page metrics using GA.

## Support
There is no support for this project and should not be used for any production use.

## License
This project is licenced using the MIT licence.

## Project status
This is a proof of concept and will not be added to in the future.